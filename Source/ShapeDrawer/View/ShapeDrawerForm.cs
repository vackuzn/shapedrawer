﻿using System;
using System.Drawing;
using System.Windows.Forms;
using ShapeDrawer.Model;

namespace ShapeDrawer.View
{
    internal partial class ShapeDrawerForm : Form
    {
        private readonly App _app;
        public ShapeDrawerForm(App app)
        {
            InitializeComponent();

            _app = app;
            _app.ShapesUpdated += AppOnShapesUpdated;
            _app.SelectedShapeChanged += SelectedShapeChanged;
        }
        private void AppOnShapesUpdated(object sender, EventArgs e)
        {
            pnlCanvas.Refresh();
        }
        private void SelectedShapeChanged(object sender, SelectedShapeChangedEventArgs e)
        {
            propertyGrid.SelectedObject = e.SelectedShape;
        }
        private void pnlCanvas_Paint(object sender, PaintEventArgs e)
        {
            Graphics graphics = e.Graphics;
            _app.Paint(graphics, GetCanvasSize());
        }
        private Size GetCanvasSize()
        {
            return new Size(pnlCanvas.Width, pnlCanvas.Height);
        }
        private void pnlCanvas_Click(object sender, EventArgs e)
        {
            if (!(e is MouseEventArgs args))
                return;

            if (args.Button == MouseButtons.Left)
                _app.SelectShapeAtLocation(args.Location);
        }

        private void pnlCanvas_Resize(object sender, EventArgs e)
        {
            pnlCanvas.Refresh();
        }

        private void ShowOpenFileDialog(object sender, EventArgs e)
        {
            using (OpenFileDialog openFileDialog = new OpenFileDialog())
            {
                openFileDialog.InitialDirectory = System.Reflection.Assembly.GetExecutingAssembly().CodeBase;
                openFileDialog.Filter = "json files (*.json)|*.json|xml files (*.xml)|*.xml";
                openFileDialog.RestoreDirectory = true;

                if (openFileDialog.ShowDialog() == DialogResult.OK)
                {
                    string filePath = openFileDialog.FileName;
                    LoadShapes(filePath);
                }
            }
        }

        public void LoadShapes(string filePath)
        {
            try
            {
                _app.LoadShapes(filePath);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message,
                    "Load shapes failed", 
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
            }
        }
    }
}
