﻿using System;
using System.Linq;
using System.Windows.Forms;
using ShapeDrawer.Model;
using ShapeDrawer.View;

namespace ShapeDrawer
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main(string[] args)
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            App app = new App();
            ShapeDrawerForm view = new ShapeDrawerForm(app);

            if (args.Any())
                view.LoadShapes(args[0]);
            
            Application.Run(view);
        }
    }
}