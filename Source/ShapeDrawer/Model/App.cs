﻿using System;
using System.Drawing;
using System.IO;
using System.Linq;
using ShapeDrawer.Model.Shapes;
using ShapeDrawer.Storage;
using ShapeDrawer.Storage.Loaders;

namespace ShapeDrawer.Model
{
    class App
    {
        public event EventHandler ShapesUpdated;
        public event EventHandler<SelectedShapeChangedEventArgs> SelectedShapeChanged;
        private ViewPort _viewPort;
        private Shape[] _shapes = new Shape[0];

        public void Paint(Graphics graphics, Size canvasSize)
        {
            _viewPort = new ViewPort(canvasSize, graphics);
            if (!_shapes.Any())
                return;
            _viewPort.Draw(_shapes);
        }

        public void LoadShapes(string filePath)
        {
            string extension = Path.GetExtension(filePath).ToLower().Trim('.');
            IShapeLoader loader = new ShapeLoaderFactory().CreateLoaderForFileType(extension);
            _shapes = loader.Load(ReadFileContent(filePath));
            ShapesUpdated?.Invoke(this, new EventArgs());
        }
        public void SelectShapeAtLocation(Point location)
        {
            Shape selected = GetShapeForLocation(location);
            SelectedShapeChanged?.Invoke(this, new SelectedShapeChangedEventArgs(selected));
        }
        private Shape GetShapeForLocation(Point location)
        {
            if (!_shapes.Any())
                return null;

            return _viewPort.GetShapeForLocation(location);
        }
        private string ReadFileContent(string filePath)
        {
            using (FileStream fileStream = File.OpenRead(filePath))
                using (StreamReader reader = new StreamReader(fileStream))
                    return reader.ReadToEnd();
        }
    }
}
