﻿using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using ShapeDrawer.Model.CoordinatesTransform;
using ShapeDrawer.Model.Shapes;

namespace ShapeDrawer.Model
{
    class ViewPort
    {
        private const int MaxLineSelectionErrorPixels = 10;
        private const int LineWidth = 3;

        private readonly Graphics _graphics;
        private readonly Size _canvasSize;
        private Shape[] _shapes;
        private ICoordinatesTransformer _transformer;
        public ViewPort(Size canvasSize, Graphics graphics)
        {
            _graphics = graphics;
            _canvasSize = new Size(canvasSize.Width-LineWidth*2, canvasSize.Height - LineWidth * 2);
        }
        public void Draw(Shape[] shapes)
        {
            _shapes = shapes;
            _transformer = CoordinatesFitter.Fit(_shapes, _canvasSize);

            foreach (Shape shape in _shapes)
                DrawShape(shape);
        }

        private void DrawShape(Shape shape)
        {
            Pen pen = GetPen(shape);
            ShapeDrawer.ShapeDrawer drawer = new ShapeDrawer.ShapeDrawer(_transformer, pen, _graphics);
            shape.Draw(drawer);
        }

        private Pen GetPen(Shape shape)
        { 
            Pen pen = new Pen(shape.Color, LineWidth);

            switch (shape.LineType)
            {
                case ELineType.Dot:
                    pen.DashStyle = DashStyle.Dot;
                    break;
                case ELineType.Dash:
                    pen.DashStyle = DashStyle.Dash;
                    break;
                case ELineType.DashDot:
                    pen.DashStyle = DashStyle.DashDot;
                    break;
            }

            return pen;
        }
        public Shape GetShapeForLocation(Point point)
        {
            PointF pointInOrigCoords = _transformer.TransformPointBack(point);
            float pixelSize = _transformer.TransformScalarBack(1);
            float lineSelectionError = pixelSize * MaxLineSelectionErrorPixels;

            Shape selected = _shapes
                .Reverse()
                .FirstOrDefault(s => s.BelongsToShape(pointInOrigCoords, lineSelectionError));

            return selected;
        }
    }
}
