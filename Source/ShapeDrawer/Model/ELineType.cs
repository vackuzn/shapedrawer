﻿namespace ShapeDrawer.Model
{
    public enum ELineType
    {
        Solid,
        Dot,
        Dash,
        DashDot
    }
}