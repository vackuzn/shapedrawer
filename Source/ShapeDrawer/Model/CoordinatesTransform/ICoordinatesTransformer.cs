﻿using System.Drawing;

namespace ShapeDrawer.Model.CoordinatesTransform
{
    internal interface ICoordinatesTransformer
    {
        float TransformScalar(float scalar);
        PointF TransformPoint(PointF point);
        PointF TransformPointBack(PointF point);
        float TransformScalarBack(float scalar);
    }
}