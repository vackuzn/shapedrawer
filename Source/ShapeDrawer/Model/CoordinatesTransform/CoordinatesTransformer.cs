﻿using System.Drawing;
using ShapeDrawer.Model.Helper;

namespace ShapeDrawer.Model.CoordinatesTransform
{
    class CoordinatesTransformer : ICoordinatesTransformer
    {
        private readonly float _scale;
        private readonly Offset _offset;
        private readonly Size _canvasSize;
        public CoordinatesTransformer(float scale, Offset offset, Size canvasSize)
        {
            _scale = scale;
            _offset = offset;
            _canvasSize = canvasSize;
        }
        public float TransformScalar(float scalar)
        {
            return scalar * _scale;
        }
        public PointF TransformPoint(PointF point)
        {
            float x = TransformX(point.X);
            float y = TransformY(point.Y);
            return new PointF(x, y);
        }
        public PointF TransformPointBack(PointF point)
        {
            float x = TransformXBack(point.X);
            float y = TransformYBack(point.Y);
            return new PointF(x, y);
        }
        public float TransformScalarBack(float scalar)
        {
            return scalar / _scale;
        }
        private float TransformX(float x)
        {
            return _scale * (x - _offset.Left);
        }
        private float TransformXBack(float x)
        {
            return x / _scale + _offset.Left;
        }
        private float TransformY(float y)
        {
            return _canvasSize.Height - _scale * (y - _offset.Bottom);
        }
        private float TransformYBack(float y)
        {
            return (_canvasSize.Height - y) / _scale + _offset.Bottom;
        }
    }
}
