﻿using System;
using System.Drawing;
using System.Linq;
using ShapeDrawer.Model.Helper;
using ShapeDrawer.Model.Shapes;

namespace ShapeDrawer.Model.CoordinatesTransform
{
    class CoordinatesFitter
    {
        private readonly Shape[] _shapes;
        private readonly Size _canvasSize;

        private float _left;
        private float _right;
        private float _bottom;
        private float _top;

        public static ICoordinatesTransformer Fit(Shape[] shapes, Size canvasSize)
        {
            if (!shapes.Any())
                throw new ArgumentException("No shapes provided");

            CoordinatesFitter fitter = new CoordinatesFitter(shapes, canvasSize);
            CoordinatesTransformer transformer = fitter.Fit();
            return transformer;
        }

        private CoordinatesFitter(Shape[] shapes, Size canvasSize)
        {
            _shapes = shapes;
            _canvasSize = canvasSize;
        }

        private CoordinatesTransformer Fit()
        {
            FitFirstShape();
            FitAllShapes();

            float scale = CalculateScale();
            Offset offset = GetOffset();
            CoordinatesTransformer transformer = CreateTransformer(scale, offset);
            return transformer;
        }
        private void FitFirstShape()
        {
            SurroundingRectangle first = _shapes.First().GetSurroundingRectangle();
            _left = first.Left;
            _right = first.Right;
            _bottom = first.Bottom;
            _top = first.Top;
        }

        private void FitAllShapes()
        {
            foreach (Shape shape in _shapes)
                FitShape(shape);
        }
        private void FitShape(Shape shape)
        {
            SurroundingRectangle rectangle = shape.GetSurroundingRectangle();
            _left = Calc.Min(rectangle.Left, _left);
            _right = Calc.Max(rectangle.Right, _right);
            _bottom = Calc.Min(rectangle.Bottom, _bottom);
            _top = Calc.Max(rectangle.Top, _top);
        }
        private float CalculateScale()
        {
            float width = _right - _left;
            float height = _top - _bottom;

            float scaleX = _canvasSize.Width / width;
            float scaleY = _canvasSize.Height / height;

            float scale = Calc.Min(scaleX, scaleY);
            return scale;
        }
        private Offset GetOffset()
        {
            return new Offset(_bottom, _left);
        }
        private CoordinatesTransformer CreateTransformer(float scale, Offset offset)
        {
            return new CoordinatesTransformer(scale, offset, _canvasSize);
        }
    }
}