﻿using System.ComponentModel;
using System.Drawing;
using System.Drawing.Drawing2D;
using ShapeDrawer.Model.Helper;
using ShapeDrawer.Model.ShapeDrawer;

namespace ShapeDrawer.Model.Shapes
{
    class Rectangle : FillableShape
    {
        private readonly SurroundingRectangle _nonTransformedRect;
        private readonly PointF _a;
        private readonly PointF _b;
        private readonly PointF _c;
        private readonly PointF _d;

        [Category(DisplayPropertiesConstants.Position)]
        public PointF Center { get; }
        [Category(DisplayPropertiesConstants.Position)]
        public float RotateAngle { get; }

        public Rectangle(Color color,
            ELineType lineType,
            bool filled, 
            PointF center,
            float width,
            float height,
            float rotateAngle) : base("Rectangle", color, lineType, filled)
        {
            RotateAngle = rotateAngle;
            Center = center;

            PointF a = new PointF(center.X - width / 2, center.Y + height / 2);
            PointF b = new PointF(center.X + width / 2, center.Y + height / 2);
            PointF c = new PointF(center.X + width / 2, center.Y - height / 2);
            PointF d = new PointF(center.X - width / 2, center.Y - height / 2);

            _nonTransformedRect = new SurroundingRectangle(a, new SizeF(width, height));

            PointF[] pts = {a, b, c, d};
            Rotate(pts, rotateAngle);

            _a = pts[0];
            _b = pts[1];
            _c = pts[2];
            _d = pts[3];
        }

        public override SurroundingRectangle GetSurroundingRectangle()
        {
            return GetRectangleForPoints(_a, _b, _c, _d);
        }
        public override void Draw(IShapeDrawer drawer)
        {
            drawer.DrawClosedPolygon(new[] {_a, _b, _c, _d}, Filled);
        }
        protected override bool BelongsToShapeSpecific(PointF point, float lineSelectionError)
        {
            PointF[] points = { point };
            Rotate(points, -RotateAngle);
            
            bool result = _nonTransformedRect.Contains(points[0]);
            return result;
        }

        private void Rotate(PointF[] points, float angle)
        {
            Matrix m = new Matrix();
            m.RotateAt(angle, Center);
            m.TransformPoints(points);
        }
    }
}