﻿using System.ComponentModel;
using System.Drawing;
using ShapeDrawer.Model.Helper;
using ShapeDrawer.Model.ShapeDrawer;

namespace ShapeDrawer.Model.Shapes
{
    class Triangle: FillableShape
    {
        [Category(DisplayPropertiesConstants.Position)]
        public PointF A { get; }
        [Category(DisplayPropertiesConstants.Position)]
        public PointF B { get; }
        [Category(DisplayPropertiesConstants.Position)]
        public PointF C { get; }

        public Triangle(
            Color color, 
            ELineType lineType,
            bool filled,
            PointF a,
            PointF b,
            PointF c) : base("Triangle", color, lineType, filled)
        {
            A = a;
            B = b;
            C = c;
        }
        public override void Draw(IShapeDrawer drawer)
        {
            drawer.DrawClosedPolygon(new[] {A, B, C}, Filled);
        }

        protected override bool BelongsToShapeSpecific(PointF point, float lineSelectionError)
        {
            double areaABC = TriangleAreaCalculator.TriangleArea(A, B, C);
            double areaPBC = TriangleAreaCalculator.TriangleArea(point, B, C);
            double areaAPC = TriangleAreaCalculator.TriangleArea(A, point, C);
            double areaABP = TriangleAreaCalculator.TriangleArea(A, B, point);

            bool pointBelongsToTriangle = areaABC >= areaPBC + areaAPC + areaABP;
            return pointBelongsToTriangle;
        }

        public override SurroundingRectangle GetSurroundingRectangle()
        {
            return GetRectangleForPoints(A, B, C);
        }
    }
}
