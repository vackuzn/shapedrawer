﻿using System;
using System.ComponentModel;
using System.Drawing;
using ShapeDrawer.Model.Helper;
using ShapeDrawer.Model.ShapeDrawer;

namespace ShapeDrawer.Model.Shapes
{
    class Line : Shape
    {
        private readonly float _k;
        private readonly float _b;
        [Category(DisplayPropertiesConstants.Position)]
        public PointF A { get; }
        [Category(DisplayPropertiesConstants.Position)]
        public PointF B { get; }
        public Line(
            Color color, 
            ELineType lineType,
            PointF a,
            PointF b) : base("Line", color, lineType)
        {
            A = a;
            B = b;

            _k = (B.Y - A.Y) / (B.X - A.X);
            _b = A.Y - _k * A.X;
        }

        public override SurroundingRectangle GetSurroundingRectangle()
        {
            return GetRectangleForPoints(A, B);
        }

        public override void Draw(IShapeDrawer drawer)
        {
            drawer.DrawLine(A, B);
        }

        protected override bool BelongsToShapeSpecific(PointF point, float lineSelectionError)
        {
            float y = _k * point.X + _b;
            return Math.Abs(point.Y - y) <= lineSelectionError;
        }
    }
}
