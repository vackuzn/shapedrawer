﻿using System.ComponentModel;
using System.Drawing;

namespace ShapeDrawer.Model.Shapes
{
    abstract class FillableShape : Shape
    {
        [Category(DisplayPropertiesConstants.Appearance)]
        public bool Filled { get; }
        protected FillableShape(string shapeType, 
            Color color, 
            ELineType lineType, 
            bool filled) : base(shapeType, color, lineType)
        {
            Filled = filled;
        }
    }
}
