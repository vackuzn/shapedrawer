﻿using System.ComponentModel;
using System.Drawing;
using System.Linq;
using ShapeDrawer.Model.Helper;
using ShapeDrawer.Model.ShapeDrawer;

namespace ShapeDrawer.Model.Shapes
{
    abstract class Shape
    {
        private const float LineWidth = 3;
        [DisplayName(DisplayPropertiesConstants.ShapeType)]
        [Category(DisplayPropertiesConstants.Id)]
        public string ShapeType { get; }
        [Category(DisplayPropertiesConstants.Appearance)]
        public Color Color { get; }
        [DisplayName(DisplayPropertiesConstants.LineType)]
        [Category(DisplayPropertiesConstants.Appearance)]
        public ELineType LineType { get; }

        protected Shape(string shapeType, Color color, ELineType lineType)
        {
            ShapeType = shapeType;
            Color = color;
            LineType = lineType;
        }
        public abstract SurroundingRectangle GetSurroundingRectangle();
        public abstract void Draw(IShapeDrawer drawer);
        protected abstract bool BelongsToShapeSpecific(PointF point, float lineSelectionError);
        public bool BelongsToShape(PointF point, float lineSelectionError)
        {
            if (!GetSurroundingRectangle().Contains(point))
                return false;

            return BelongsToShapeSpecific(point, lineSelectionError);
        }
        protected SurroundingRectangle GetRectangleForPoints(params PointF[] points)
        {
            float minXValue = Calc.Min(points.Select(p => p.X));
            float maxYValue = Calc.Max(points.Select(p => p.Y));
            PointF topLeftCorner = new PointF(minXValue, maxYValue);

            float width = Calc.DifferenceBetweenMaxAndMin(points.Select(p => p.X));
            float height = Calc.DifferenceBetweenMaxAndMin(points.Select(p => p.Y));
            SizeF size = new SizeF(width, height);

            return new SurroundingRectangle(topLeftCorner, size);
        }
    }
}
