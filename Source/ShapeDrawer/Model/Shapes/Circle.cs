﻿using System;
using System.ComponentModel;
using System.Drawing;
using ShapeDrawer.Model.Helper;
using ShapeDrawer.Model.ShapeDrawer;

namespace ShapeDrawer.Model.Shapes
{
    class Circle : FillableShape
    {
        [Category(DisplayPropertiesConstants.Position)]
        public PointF Center { get; }
        [Category(DisplayPropertiesConstants.Position)]
        public float Radius { get; }

        public Circle(
            Color color, 
            ELineType lineType,
            bool filled,
            PointF center, 
            float radius) : base("Circle", color, lineType, filled)
        {
            Center = center;
            Radius = radius;
        }

        public override SurroundingRectangle GetSurroundingRectangle()
        {
            PointF topRightCorner = new PointF(Center.X+Radius, Center.Y+Radius);
            PointF bottomLeftCorner = new PointF(Center.X - Radius, Center.Y - Radius);

            return GetRectangleForPoints(topRightCorner, bottomLeftCorner);
        }

        public override void Draw(IShapeDrawer drawer)
        {
            SurroundingRectangle rectangle = GetSurroundingRectangle();
            drawer.DrawCircle(rectangle.TopLeft, Radius, Filled);
        }

        protected override bool BelongsToShapeSpecific(PointF point, float lineSelectionError)
        {
            double pointDistanceFromCenter = Math.Pow(point.X - Center.X, 2) + Math.Pow(point.Y - Center.Y, 2);
            bool belongsToShape = pointDistanceFromCenter <= Math.Pow(Radius, 2);
            return belongsToShape;
        }
    }
}
