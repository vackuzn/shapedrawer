﻿using System;
using ShapeDrawer.Model.Shapes;

namespace ShapeDrawer.Model
{
    class SelectedShapeChangedEventArgs : EventArgs
    {
        public Shape SelectedShape { get; }
        public SelectedShapeChangedEventArgs(Shape selected)
        {
            SelectedShape = selected;
        }
    }
}