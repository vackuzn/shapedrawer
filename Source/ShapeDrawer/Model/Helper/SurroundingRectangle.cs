﻿using System.Drawing;

namespace ShapeDrawer.Model.Helper
{
    class SurroundingRectangle
    {
        private readonly PointF _topLeftCorner;
        private readonly SizeF _size;

        public PointF TopLeft => new PointF(Left, Top);

        public float Left => _topLeftCorner.X;
        public float Right => _topLeftCorner.X + _size.Width;
        public float Top => _topLeftCorner.Y;
        public float Bottom => _topLeftCorner.Y - _size.Height;

        public SurroundingRectangle(PointF topLeftCorner, SizeF size)
        {
            _topLeftCorner = topLeftCorner;
            _size = size;
        }
        public bool Contains(PointF point)
        {
            bool xWithinRect = Left <= point.X && point.X <= Right;
            bool yWithinRect = Bottom <= point.Y && point.Y <= Top;

            return xWithinRect & yWithinRect;
        }
    }
}
