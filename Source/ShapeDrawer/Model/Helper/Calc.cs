﻿using System.Collections.Generic;
using System.Linq;

namespace ShapeDrawer.Model.Helper
{
    static class Calc
    {
        public static float Max(params float[] args)
        {
            return args.Max();
        }

        public static float Min(params float[] args)
        {
            return args.Min();
        }
        public static float Min(IEnumerable<float> args)
        {
            return Min(args.ToArray());
        }
        public static float Max(IEnumerable<float> args)
        {
            return Max(args.ToArray());
        }
        internal static float DifferenceBetweenMaxAndMin(IEnumerable<float> enumerable)
        {
            float[] args = enumerable.ToArray();
            float max = Max(args);
            float min = Min(args);

            return max - min;
        }
    }
}
