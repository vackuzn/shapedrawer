﻿using System;
using System.Drawing;

namespace ShapeDrawer.Model.Helper
{
    class TriangleAreaCalculator
    {
        public static double TriangleArea(PointF p1, PointF p2, PointF p3)
        {
            return Math.Abs((p1.X * (p2.Y - p3.Y) +
                             p2.X * (p3.Y - p1.Y) +
                             p3.X * (p1.Y - p2.Y)) / 2.0);
        }
    }
}
