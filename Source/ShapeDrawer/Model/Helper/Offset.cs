﻿namespace ShapeDrawer.Model.Helper
{
    class Offset
    {
        public float Left { get; }
        public float Bottom { get; }

        public Offset(float bottom, float left)
        {
            Left = left;
            Bottom = bottom;
        }
    }
}
