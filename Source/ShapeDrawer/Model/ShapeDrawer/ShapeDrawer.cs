﻿using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using ShapeDrawer.Model.CoordinatesTransform;

namespace ShapeDrawer.Model.ShapeDrawer
{
    class ShapeDrawer : IShapeDrawer
    {
        private readonly ICoordinatesTransformer _transformer;
        private readonly Pen _pen;
        private readonly Graphics _graphics;

        public ShapeDrawer(ICoordinatesTransformer transformer, Pen pen, Graphics graphics)
        {
            _pen = pen;
            _transformer = transformer;
            _graphics = graphics;
        }
        public void DrawLine(PointF a, PointF b)
        {
            PointF aTr = _transformer.TransformPoint(a);
            PointF bTr = _transformer.TransformPoint(b);

            GraphicsPath linePath = new GraphicsPath();
            linePath.AddLine(aTr, bTr);

            Draw(linePath, false);
        }
        public void DrawCircle(PointF topLeft, float radius, bool filled)
        {
            PointF topLeftTr = _transformer.TransformPoint(topLeft);
            float diameterTr = _transformer.TransformScalar(radius * 2);

            GraphicsPath circlePath = new GraphicsPath();
            circlePath.AddEllipse(topLeftTr.X, topLeftTr.Y, diameterTr, diameterTr);

            Draw(circlePath, filled);
        }

        public void DrawClosedPolygon(PointF[] pts, bool filled)
        {
            List<PointF> points = ClosePolygonIfNotClosed(pts);
            PointF[] transformedPoints = points.Select(_transformer.TransformPoint).ToArray();

            GraphicsPath polygonPath = new GraphicsPath();
            polygonPath.AddLines(transformedPoints);

            Draw(polygonPath, filled);
        }
        private List<PointF> ClosePolygonIfNotClosed(IEnumerable<PointF> pts)
        {
            List<PointF> points = new List<PointF>(pts);
            if (points.First() != points.Last())
                points.Add(points.First());

            return points;
        }

        private void Draw(GraphicsPath path, bool filled)
        {
            if (filled)
                _graphics.FillPath(GetBrush(), path);
            else
                _graphics.DrawPath(_pen, path);
        }
        private Brush GetBrush()
        {
            return new SolidBrush(_pen.Color);
        }
    }
}
