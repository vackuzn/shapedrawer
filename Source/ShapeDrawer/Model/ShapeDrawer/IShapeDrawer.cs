﻿using System.Drawing;

namespace ShapeDrawer.Model.ShapeDrawer
{
    interface IShapeDrawer
    {
        void DrawLine(PointF a, PointF b);
        void DrawCircle(PointF topLeft, float radius, bool filled);
        void DrawClosedPolygon(PointF[] pts, bool filled);
    }
}
