﻿namespace ShapeDrawer.Model
{
    class DisplayPropertiesConstants
    {
        public const string Position = "Position";
        public const string Appearance = "Appearance";
        public const string Id = "Id";
        public const string LineType = "Line Type";
        public const string ShapeType = "Shape Type";
    }
}
