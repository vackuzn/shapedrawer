﻿using System;
using System.Drawing;
using System.Linq;
using ShapeDrawer.Model;

namespace ShapeDrawer.Storage.Loaders.FieldsParser
{
    class FieldsParser
    {
        public ELineType ParseLineType(string lineType)
        {
            ThrowOnNullOrEmptyInputString(lineType, "Line type string cannot be null or empty");

            switch (lineType.ToLower())
            {
                case "solid":
                    return ELineType.Solid;
                case "dot":
                    return ELineType.Dot;
                case "dash":
                    return ELineType.Dash;
                case "dashdot":
                    return ELineType.DashDot;
                default:
                    throw new ParseException("Unknown line type " + lineType);
            }
        }
        public PointF ParseCoords(string point)
        {
            ThrowOnNullOrEmptyInputString(point, "Coordinates string cannot be null or empty");

            float[] coords = TryParseCoordinateString(point);

            float x = coords[0];
            float y = coords[1];
            return new PointF(x, y);
        }

        public Color ParseColor(string color)
        {
            ThrowOnNullOrEmptyInputString(color, "Color string cannot be null or empty");
            int[] argb = TryParseColorString(color);
            return Color.FromArgb(argb[0], argb[1], argb[2], argb[3]);
        }
        private void ThrowOnNullOrEmptyInputString(string input, string errorMessage)
        {
            if (string.IsNullOrEmpty(input))
                throw new ArgumentException(errorMessage);
        }

        private float[] TryParseCoordinateString(string coordsString)
        {
            float[] coords;
            try
            {
                coords = coordsString.Split(';')
                    .Select(s => s.Trim())
                    .Select(float.Parse)
                    .ToArray();
            }
            catch (Exception ex)
            {
                throw new ParseException($"Unexpected point coordinates string '{coordsString}'", ex);
            }

            if (coords.Length != 2)
                throw new ParseException($"Unexpected point format '{coordsString}'");

            return coords;
        }
        private int[] TryParseColorString(string color)
        {
            int[] argb;
            try
            {
                argb = color.Split(';')
                    .Select(s => s.Trim())
                    .Select(Int32.Parse)
                    .ToArray();
            }
            catch (FormatException ex)
            {
                throw new ParseException($"Unexpected color string '{color}'", ex);
            }

            if (argb.Length != 4)
                throw new ParseException($"Unexpected color string '{color}'");

            return argb;
        }
    }
}
