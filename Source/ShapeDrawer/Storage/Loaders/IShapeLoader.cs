﻿using ShapeDrawer.Model.Shapes;

namespace ShapeDrawer.Storage.Loaders
{
    interface IShapeLoader
    {
        Shape[] Load(string str);
    }
}
