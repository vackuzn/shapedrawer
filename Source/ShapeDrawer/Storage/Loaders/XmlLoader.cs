﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Xml.Linq;
using ShapeDrawer.Model;
using ShapeDrawer.Model.Shapes;
using Rectangle = ShapeDrawer.Model.Shapes.Rectangle;

namespace ShapeDrawer.Storage.Loaders
{
    class XmlLoader : IShapeLoader
    {
        private readonly FieldsParser.FieldsParser _parser = new FieldsParser.FieldsParser();

        public Shape[] Load(string xml)
        {
            if (string.IsNullOrEmpty(xml))
                throw new ArgumentException("Xml string cannot be null or empty");

            List<Shape> result = new List<Shape>();

            XDocument doc = XDocument.Parse(xml);
            foreach (XElement node in doc.Root.Elements())
            {
                Shape shape = ParseShape(node);
                result.Add(shape);
            }

            return result.ToArray();
        }

        private Shape ParseShape(XElement node)
        {
            string shapeName = node.Name.LocalName;
            switch (shapeName)
            {
                case "line":
                    Line line = ParseLine(node);
                    return line;
                case "circle":
                    Circle circle = ParseCircle(node);
                    return circle;
                case "triangle":
                    Triangle triangle = ParseTriangle(node);
                    return triangle;
                case "rectangle":
                    Rectangle rectangle = ParseRectangle(node);
                    return rectangle;
                default:
                    throw new ParseException("Unknown shape " + shapeName);
            }
        }

        private Rectangle ParseRectangle(XElement node)
        {
            Color color = _parser.ParseColor(GetAttributeValue(node,"color"));
            ELineType lineType = _parser.ParseLineType(GetAttributeValue(node,"lineType"));
            bool filled = bool.Parse(GetAttributeValue(node,"filled"));
            PointF center = _parser.ParseCoords(GetAttributeValue(node,"center"));
            float width = float.Parse(GetAttributeValue(node,"width"));
            float height = float.Parse(GetAttributeValue(node,"height"));
            float rotateAngle = float.Parse(GetAttributeValue(node,"rotateAngle"));

            return new Rectangle(color, lineType, filled, center, width, height, rotateAngle);
        }

        private Triangle ParseTriangle(XElement node)
        {
            Color color = _parser.ParseColor(GetAttributeValue(node,"color"));
            ELineType lineType = _parser.ParseLineType(GetAttributeValue(node,"lineType"));
            bool filled = bool.Parse(GetAttributeValue(node,"filled"));
            PointF a = _parser.ParseCoords(GetAttributeValue(node,"a"));
            PointF b = _parser.ParseCoords(GetAttributeValue(node,"b"));
            PointF c = _parser.ParseCoords(GetAttributeValue(node,"c"));

            return new Triangle(color, lineType, filled, a, b, c);
        }

        private Circle ParseCircle(XElement node)
        {
            Color color = _parser.ParseColor(GetAttributeValue(node,"color"));
            ELineType lineType = _parser.ParseLineType(GetAttributeValue(node,"lineType"));
            bool filled = bool.Parse(GetAttributeValue(node,"filled"));
            PointF center = _parser.ParseCoords(GetAttributeValue(node,"center"));
            float radius = float.Parse(GetAttributeValue(node,"radius"));

            return new Circle(color, lineType, filled, center, radius);
        }

        private Line ParseLine(XElement node)
        {
            var color = _parser.ParseColor(GetAttributeValue(node, "color"));
            var lineType = _parser.ParseLineType(GetAttributeValue(node,"lineType"));
            var a = _parser.ParseCoords(GetAttributeValue(node,"a"));
            var b = _parser.ParseCoords(GetAttributeValue(node,"b"));

            return new Line(color, lineType, a, b);
        }

        private string GetAttributeValue(XElement node, string attributeName)
        {
            XAttribute attribute = node.Attribute(attributeName);
            if (attribute == null)
                throw new ParseException($"Missing required field '{attributeName}'");

            return attribute.Value;
        }
    }
}
