﻿using System;
using System.Collections.Generic;
using System.Drawing;
using Newtonsoft.Json.Linq;
using ShapeDrawer.Model;
using ShapeDrawer.Model.Shapes;
using Rectangle = ShapeDrawer.Model.Shapes.Rectangle;

namespace ShapeDrawer.Storage.Loaders
{
    class JsonLoader : IShapeLoader
    {
        private readonly FieldsParser.FieldsParser _parser = new FieldsParser.FieldsParser();

        public Shape[] Load(string json)
        {
            if (string.IsNullOrEmpty(json))
                throw new ArgumentException("Json string cannot be null or empty");

            List<Shape> result = new List<Shape>();

            dynamic shapes = JArray.Parse(json);
            foreach (dynamic shapeNode in shapes)
            {
                string type = shapeNode["type"];
                switch (type)
                {
                    case "line":
                        result.Add(ParseLine(shapeNode));
                        break;
                    case "circle":
                        result.Add(ParseCircle(shapeNode));
                        break;
                    case "triangle":
                        result.Add(ParseTriangle(shapeNode));
                        break;
                    case "rectangle":
                        result.Add(ParseRectangle(shapeNode));
                        break;
                    default:
                        throw new ParseException("Unknown shape type " + type);
                }
            }

            return result.ToArray();
        }
        private Line ParseLine(dynamic shapeNode)
        {
            Color color = _parser.ParseColor(ReadValue<string>(shapeNode, "color"));
            ELineType lineType = _parser.ParseLineType(ReadValue<string>(shapeNode, "lineType"));
            PointF a = _parser.ParseCoords(ReadValue<string>(shapeNode, "a"));
            PointF b = _parser.ParseCoords(ReadValue<string>(shapeNode, "b"));

            return new Line(color, lineType, a, b);
        }
        private Circle ParseCircle(dynamic shapeNode)
        {
            Color color = _parser.ParseColor(ReadValue<string>(shapeNode, "color"));
            ELineType lineType = _parser.ParseLineType(ReadValue<string>(shapeNode, "lineType"));
            bool filled = ReadValue<bool>(shapeNode, "filled");
            PointF center = _parser.ParseCoords(ReadValue<string>(shapeNode, "center"));
            float radius = ReadValue<float>(shapeNode, "radius");

            return new Circle(color, lineType, filled, center, radius);
        }
        private Triangle ParseTriangle(dynamic shapeNode)
        {
            Color color = _parser.ParseColor(ReadValue<string>(shapeNode, "color"));
            ELineType lineType = _parser.ParseLineType(ReadValue<string>(shapeNode, "lineType"));
            bool filled = ReadValue<bool>(shapeNode, "filled");
            PointF a = _parser.ParseCoords(ReadValue<string>(shapeNode, "a"));
            PointF b = _parser.ParseCoords(ReadValue<string>(shapeNode, "b"));
            PointF c = _parser.ParseCoords(ReadValue<string>(shapeNode, "c"));

            return new Triangle(color, lineType, filled, a, b, c);
        }
        private Rectangle ParseRectangle(dynamic shapeNode)
        {
            Color color = _parser.ParseColor(ReadValue<string>(shapeNode,"color"));
            ELineType lineType = _parser.ParseLineType(ReadValue<string>(shapeNode, "lineType"));
            bool filled = ReadValue<bool>(shapeNode, "filled");
            PointF center = _parser.ParseCoords(ReadValue<string>(shapeNode, "center"));
            float width = ReadValue<float>(shapeNode, "width");
            float height = ReadValue<float>(shapeNode,"height");
            float angle = ReadValue<float>(shapeNode,"rotateAngle");

            return new Rectangle(color, lineType, filled, center, width, height, angle);
        }

        private T ReadValue<T>(dynamic node, string name)
        {
            object value = node[name];
            if (value == null)
                throw new ParseException($"Required field '{name}' is missing");

            try
            {
                return (T) Convert.ChangeType(value, typeof(T));
            }
            catch (FormatException ex)
            {
                throw new ParseException($"Field '{name}' has unexpected value of '{value}'", ex);
            }
        }
    }
}
