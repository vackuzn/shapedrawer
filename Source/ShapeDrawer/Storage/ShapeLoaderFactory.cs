﻿using System;
using ShapeDrawer.Storage.Loaders;

namespace ShapeDrawer.Storage
{
    class ShapeLoaderFactory
    {
        public IShapeLoader CreateLoaderForFileType(string fileType)
        {
            switch (fileType)
            {
                case "json":
                    return new JsonLoader();
                case "xml":
                    return new XmlLoader();
                default:
                    throw new ArgumentException("Unsupported file type " + fileType);
            }
        }
    }
}
