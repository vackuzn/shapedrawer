﻿using System;
using System.Drawing;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ShapeDrawer.Model.CoordinatesTransform;
using ShapeDrawer.Model.Helper;

namespace ShapeDrawerTests
{
    [TestClass]
    public class CoordinatesTransformerTests
    {
        private CoordinatesTransformer BuildTransformer()
        {
            CoordinatesTransformer transformer = new CoordinatesTransformer(
                0.4f,
                new Offset(-15.6f, 12.11f),
                new Size(300, 500));

            return transformer;
        }

        private bool PointsEqual(PointF p1, PointF p2)
        {
            var epsilon = 0.0001;
            bool xEqual = Math.Abs(p1.X - p2.X) < epsilon;
            bool yEqual = Math.Abs(p1.Y - p2.Y) < epsilon;

            return xEqual & yEqual;
        }

        [TestMethod]
        public void TransformThereAndBackReturnsSameResult()
        {
            //Arrange
            var transformer = BuildTransformer();
            PointF point = new PointF(15.3f, -21.6f);

            //Act
            PointF transformed = transformer.TransformPoint(point);
            PointF transformedBack = transformer.TransformPointBack(transformed);

            //Assert
            Assert.IsTrue(PointsEqual(point, transformedBack));
        }
    }
}
