﻿using System;
using System.Drawing;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ShapeDrawer.Model;
using ShapeDrawer.Model.Shapes;
using ShapeDrawer.Storage;
using ShapeDrawer.Storage.Loaders;

namespace ShapeDrawerTests.Loaders
{
    [TestClass]
    public class JsonLoaderTests
    {
        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void ThrowsIfNullJsonPassed()
        {
            //Arrange
            JsonLoader jsonLoader = new JsonLoader();

            //Act
            jsonLoader.Load(null);

            //Assert
            Assert.Fail("No exception thrown for null json input");
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void ThrowsIfEmptyJsonPassed()
        {
            //Arrange
            JsonLoader jsonLoader = new JsonLoader();

            //Act
            jsonLoader.Load(string.Empty);

            //Assert
            Assert.Fail("No exception thrown for empty json string");
        }

        [TestMethod]
        public void EmptyShapesJsonArrayReturnsEmptyShapesArray()
        {
            //Arrange
            JsonLoader jsonLoader = new JsonLoader();

            //Act
            Shape[] shapes = jsonLoader.Load("[]");

            //Assert
            Assert.AreEqual(shapes.Length, 0);
        }


        [TestMethod]
        public void LoadsLineCorrectly()
        {
            //Arrange
            string lineJson = @"
            [
                {
                ""type"": ""line"",
                ""a"": ""-1,5; 3,4"",
                ""b"": ""2,2; 5,7"",
                ""color"": ""127; 255; 255; 255"",
                ""lineType"": ""solid""
                }
            ]";
            JsonLoader jsonLoader = new JsonLoader();

            //Act
            Shape[] shapes = jsonLoader.Load(lineJson);
            Line line = shapes.Single() as Line;

            //Assert
            Assert.IsNotNull(line, "Parsed shape is not line");

            Assert.AreEqual(Color.FromArgb(127, 255, 255, 255), line.Color);
            Assert.AreEqual(ELineType.Solid, line.LineType);
            Assert.AreEqual(new PointF(-1.5f, 3.4f), line.A);
            Assert.AreEqual(new PointF(2.2f, 5.7f), line.B);
        }

        [TestMethod]
        public void LoadsCircleCorrectly()
        {
            //Arrange
            string circleJson = @"
            [
                {
                ""type"": ""circle"",
                ""center"": ""0; 0"",
                ""radius"": 15.0,
                ""filled"": false,
                ""color"": ""127; 255; 0; 0"",
                ""lineType"": ""dot""
                }
            ]";
            JsonLoader jsonLoader = new JsonLoader();

            //Act
            Shape[] shapes = jsonLoader.Load(circleJson);
            Circle circle = shapes.Single() as Circle;

            //Assert
            Assert.IsNotNull(circle, "Parsed shape is not Circle");

            Assert.AreEqual(Color.FromArgb(127, 255, 0, 0), circle.Color);
            Assert.AreEqual(ELineType.Dot, circle.LineType);
            Assert.AreEqual(false, circle.Filled);
            Assert.AreEqual(new PointF(0, 0), circle.Center);
            Assert.AreEqual(15.0, circle.Radius);
        }

        [TestMethod]
        public void LoadsTriangleCorrectly()
        {
            //Arrange
            string triangleJson = @"
            [
                {
                ""type"": ""triangle"",
                ""a"": ""-15; -20"",
                ""b"": ""15; -20,3"",
                ""c"": ""0; 21"",
                ""filled"": true,
                ""color"": ""127; 255; 0; 255"",
                ""lineType"": ""dashDot""
                }
            ]";

            JsonLoader jsonLoader = new JsonLoader();

            //Act
            Shape[] shapes = jsonLoader.Load(triangleJson);
            Triangle triangle = shapes.Single() as Triangle;

            //Assert
            Assert.IsNotNull(triangle, "Parsed shape is not Triangle");

            Assert.AreEqual(Color.FromArgb(127, 255, 0, 255), triangle.Color);
            Assert.AreEqual(ELineType.DashDot, triangle.LineType);
            Assert.AreEqual(true, triangle.Filled);
            Assert.AreEqual(new PointF(-15, -20), triangle.A);
            Assert.AreEqual(new PointF(15, -20.3f), triangle.B);
            Assert.AreEqual(new PointF(0, 21), triangle.C);
        }
        [TestMethod]
        public void LoadsRectangleCorrectly()
        {
            //Arrange
            string rectangleJson = @"
            [
                {
                ""type"": ""rectangle"",
                ""color"": ""127; 255; 0; 255"",
                ""lineType"": ""dashDot"",
                ""filled"": true,
                ""center"": ""10; 10"",
                ""width"": ""20"",
                ""height"": ""30"",
                ""rotateAngle"": ""15""
                }
            ]";
            JsonLoader jsonLoader = new JsonLoader();

            //Act
            Shape[] shapes = jsonLoader.Load(rectangleJson);
            ShapeDrawer.Model.Shapes.Rectangle rectangle = shapes.Single() as ShapeDrawer.Model.Shapes.Rectangle;

            //Assert
            Assert.IsNotNull(rectangle, "Parsed shape is not Triangle");

            Assert.AreEqual(Color.FromArgb(127, 255, 0, 255), rectangle.Color);
            Assert.AreEqual(ELineType.DashDot, rectangle.LineType);
            Assert.AreEqual(true, rectangle.Filled);
            Assert.AreEqual(new PointF(10, 10), rectangle.Center);
            Assert.AreEqual(15, rectangle.RotateAngle);
        }

        [TestMethod]
        public void LoadsMultipleShapesCorrectly()
        {
            //Arrange
            string fullJson = @"
            [
                {
                ""type"": ""line"",
                ""a"": ""-1,5; 3,4"",
                ""b"": ""2,2; 5,7"",
                ""color"": ""127; 255; 255; 255"",
                ""lineType"": ""solid""
                },
                {
                ""type"": ""circle"",
                ""center"": ""0; 0"",
                ""radius"": 15.0,
                ""filled"": false,
                ""color"": ""127; 255; 0; 0"",
                ""lineType"": ""dot""
                },
                {
                ""type"": ""triangle"",
                ""a"": ""-15; -20"",
                ""b"": ""15; -20,3"",
                ""c"": ""0; 21"",
                ""filled"": true,
                ""color"": ""127; 255; 0; 255"",
                ""lineType"": ""dashDot""
                },
                {
                ""type"": ""rectangle"",
                ""color"": ""127; 255; 0; 255"",
                ""lineType"": ""dashDot"",
                ""filled"": true,
                ""center"": ""10; 10"",
                ""width"": ""20"",
                ""height"": ""30"",
                ""rotateAngle"": ""0""
                }
            ]";

            JsonLoader jsonLoader = new JsonLoader();

            //Act
            Shape[] shapes = jsonLoader.Load(fullJson);

            //Assert
            Assert.AreEqual(shapes.Length, 4);

            Assert.IsInstanceOfType(shapes[0], typeof(Line));
            Assert.IsInstanceOfType(shapes[1], typeof(Circle));
            Assert.IsInstanceOfType(shapes[2], typeof(Triangle));
            Assert.IsInstanceOfType(shapes[3], typeof(ShapeDrawer.Model.Shapes.Rectangle));
        }

        [TestMethod]
        [ExpectedException(typeof(ParseException))]
        public void RequiredFieldMissing()
        {
            //Arrange
            //width field missing
            string rectangleJson = @"
            [
                {
                ""type"": ""rectangle"",
                ""color"": ""127; 255; 0; 255"",
                ""filled"": true,
                ""lineType"": ""dashDot"",
                ""center"": ""10; 10"",
                ""height"": ""30"",
                ""rotateAngle"": ""0""
                }
            ]";
            JsonLoader jsonLoader = new JsonLoader();

            //Act
            Shape[] shapes = jsonLoader.Load(rectangleJson);

            //Assert
            Assert.Fail("No exception thrown for missing field");
        }
    }
}