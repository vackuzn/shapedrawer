﻿using System;
using System.Drawing;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ShapeDrawer.Storage;

namespace ShapeDrawerTests.Loaders.FieldsParser
{
    [TestClass]
    public class ParseCoordsTests
    {
        [TestMethod]
        public void ParsesCoordsCorrectly()
        {
            //Arrange
            ShapeDrawer.Storage.Loaders.FieldsParser.FieldsParser parser = new ShapeDrawer.Storage.Loaders.FieldsParser.FieldsParser();

            //Act
            PointF point = parser.ParseCoords("15; -20");

            //Assert
            Assert.AreEqual(point, new PointF(15, -20));
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void ThrowsNullCoordsString()
        {
            //Arrange
            ShapeDrawer.Storage.Loaders.FieldsParser.FieldsParser parser = new ShapeDrawer.Storage.Loaders.FieldsParser.FieldsParser();

            //Act
            parser.ParseCoords(null);

            //Assert
            Assert.Fail("Exception not thrown for null input");
        }

        [TestMethod]
        [ExpectedException(typeof(ParseException))]
        public void ThrowsIncorrectCoordsString()
        {
            //Arrange
            ShapeDrawer.Storage.Loaders.FieldsParser.FieldsParser parser = new ShapeDrawer.Storage.Loaders.FieldsParser.FieldsParser();

            //Act
            PointF point = parser.ParseCoords("incorrect");

            //Assert
            Assert.Fail("No exception thrown for incorrect coordinate string");
        }

        [TestMethod]
        [ExpectedException(typeof(ParseException))]
        public void ThrowsIncorrectSplitCoordsString()
        {
            //Arrange
            ShapeDrawer.Storage.Loaders.FieldsParser.FieldsParser parser = new ShapeDrawer.Storage.Loaders.FieldsParser.FieldsParser();

            //Act
            parser.ParseCoords("12;44;55");

            //Assert
            Assert.Fail("No exception thrown for incorrect coordinate string");
        }
    }
}
