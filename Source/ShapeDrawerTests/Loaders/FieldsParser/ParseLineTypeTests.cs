﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ShapeDrawer.Model;
using ShapeDrawer.Storage;

namespace ShapeDrawerTests.Loaders.FieldsParser
{
    [TestClass]
    public class ParseLineTypeTests
    {
        [TestMethod]
        public void ParseCorrectLineType()
        {
            //Arrange
            ShapeDrawer.Storage.Loaders.FieldsParser.FieldsParser parser = new ShapeDrawer.Storage.Loaders.FieldsParser.FieldsParser();

            //Act
            var lineType = parser.ParseLineType("solid");

            //Assert
            Assert.AreEqual(lineType, ELineType.Solid);
        }

        [TestMethod]
        [ExpectedException(typeof(ParseException))]
        public void ThrowsOnIncorrectLineType()
        {
            //Arrange
            ShapeDrawer.Storage.Loaders.FieldsParser.FieldsParser parser = new ShapeDrawer.Storage.Loaders.FieldsParser.FieldsParser();

            //Act
            parser.ParseLineType("notextist");

            //Assert
            Assert.Fail("No exception thrown for invalid line type");
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void ThrowsOnNullInput()
        {
            //Arrange
            ShapeDrawer.Storage.Loaders.FieldsParser.FieldsParser parser = new ShapeDrawer.Storage.Loaders.FieldsParser.FieldsParser();

            //Act
            parser.ParseLineType(null);

            //Assert
            Assert.Fail("No exception thrown for invalid line type");
        }
    }
}
