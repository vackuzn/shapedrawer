﻿using System;
using System.Drawing;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ShapeDrawer.Storage;

namespace ShapeDrawerTests.Loaders.FieldsParser
{
    [TestClass]
    public class ParseColorTests
    {
        [TestMethod]
        public void ParsesColorCorrectly()
        {
            //Arrange
            ShapeDrawer.Storage.Loaders.FieldsParser.FieldsParser parser = new ShapeDrawer.Storage.Loaders.FieldsParser.FieldsParser();

            //Act
            Color color = parser.ParseColor("127; 255; 0; 0");

            //Assert
            Assert.AreEqual(color, Color.FromArgb(127, 255, 0, 0));
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void ThrowsOnNullColorString()
        {
            //Arrange
            ShapeDrawer.Storage.Loaders.FieldsParser.FieldsParser parser = new ShapeDrawer.Storage.Loaders.FieldsParser.FieldsParser();

            //Act
            Color color = parser.ParseColor(null);

            //Assert
            Assert.Fail("No exception thrown for incorrect color string");
        }

        [TestMethod]
        [ExpectedException(typeof(ParseException))]
        public void ThrowsOnNotColorString()
        {
            //Arrange
            ShapeDrawer.Storage.Loaders.FieldsParser.FieldsParser parser = new ShapeDrawer.Storage.Loaders.FieldsParser.FieldsParser();

            //Act
            Color color = parser.ParseColor("notcolor");

            //Assert
            Assert.Fail("No exception thrown for incorrect color string");
        }

        [TestMethod]
        [ExpectedException(typeof(ParseException))]
        public void ThrowsOnIncorrectSplitColorString()
        {
            //Arrange
            ShapeDrawer.Storage.Loaders.FieldsParser.FieldsParser parser = new ShapeDrawer.Storage.Loaders.FieldsParser.FieldsParser();

            //Act
            Color color = parser.ParseColor("127; 255; 0");

            //Assert
            Assert.Fail("No exception thrown for incorrect color string");
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void ThrowsOnOutOfRangeColorValues()
        {
            //Arrange
            ShapeDrawer.Storage.Loaders.FieldsParser.FieldsParser parser = new ShapeDrawer.Storage.Loaders.FieldsParser.FieldsParser();

            //Act
            parser.ParseColor("0;1222;44;55");

            //Assert
            Assert.Fail("No exception thrown for out of range coordinate string");
        }
    }
}
