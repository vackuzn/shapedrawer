﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using ShapeDrawer.Model;

namespace ShapeDrawerTests.Loaders
{
    [TestClass]
    public class FieldParserTests
    {




        [TestMethod]
        public void ParsesLineTypeCorrectly()
        {
            //Arrange
            ShapeDrawer.Storage.Loaders.FieldsParser.FieldsParser parser = new ShapeDrawer.Storage.Loaders.FieldsParser.FieldsParser();

            //Act
            ELineType lineType = parser.ParseLineType("dot");

            //Assert
            Assert.AreEqual(lineType, ELineType.Dot);
        }
    }
}
