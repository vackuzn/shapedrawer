﻿using System;
using System.Drawing;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ShapeDrawer.Model;
using ShapeDrawer.Model.Shapes;
using ShapeDrawer.Storage;
using ShapeDrawer.Storage.Loaders;

namespace ShapeDrawerTests.Loaders
{
    [TestClass]
    public class XmlLoaderTests
    {
        [TestMethod]
        public void LoadsLineCorrectly()
        {
            string xml = @"
                <shapes>
                    <line color=""127; 255; 255; 255"" lineType=""solid"" a=""-1,5; 3,4"" b=""2,2; 5,7"" />
                </shapes>";

            XmlLoader loader = new XmlLoader();
            Shape[] shapes = loader.Load(xml);

            Line line = shapes.Single() as Line;
            Assert.IsNotNull(line);

            Assert.AreEqual(line.Color, Color.FromArgb(127, 255, 255, 255));
            Assert.AreEqual(line.LineType, ELineType.Solid);
            Assert.AreEqual(line.A, new PointF(-1.5f, 3.4f));
            Assert.AreEqual(line.B, new PointF(2.2f, 5.7f));
        }

        [TestMethod]
        public void LoadsCircleCorrectly()
        {
            //Arrange
            string xml = @"
                <shapes>
                    <circle color=""127; 255; 255; 255"" lineType=""solid"" filled=""false"" center=""2,2; 5,7"" radius=""10"" />
                </shapes>";
            XmlLoader loader = new XmlLoader();

            //Act
            Shape[] shapes = loader.Load(xml);
            Circle circle = shapes.Single() as Circle;

            //Assert
            Assert.IsNotNull(circle);

            Assert.AreEqual(Color.FromArgb(127, 255, 255, 255), circle.Color);
            Assert.AreEqual(ELineType.Solid, circle.LineType);
            Assert.AreEqual(false, circle.Filled);
            Assert.AreEqual(new PointF(2.2f, 5.7f), circle.Center);
            Assert.AreEqual(10, circle.Radius);
        }

        [TestMethod]
        public void LoadsTriangleCorrectly()
        {
            //Arrange
            string xml = @"
                <shapes>
                    <triangle color=""127; 255; 255; 255"" lineType=""solid"" filled=""true"" a=""-15; -20"" b=""15; -20,3"" c=""0; 21"" />
                </shapes>";
            XmlLoader loader = new XmlLoader();

            //Act
            Shape[] shapes = loader.Load(xml);
            Triangle triangle = shapes.Single() as Triangle;

            //Assert
            Assert.IsNotNull(triangle);

            Assert.AreEqual(Color.FromArgb(127, 255, 255, 255), triangle.Color);
            Assert.AreEqual(ELineType.Solid, triangle.LineType);
            Assert.AreEqual(true, triangle.Filled);
            Assert.AreEqual(new PointF(-15, -20), triangle.A);
            Assert.AreEqual(new PointF(15, -20.3f), triangle.B);
            Assert.AreEqual(new PointF(0, 21), triangle.C);
        }

        [TestMethod]
        public void LoadsRectangleCorrectly()
        {
            //Arrange
            string xml = @"
                <shapes>
                    <rectangle color=""127; 255; 0; 255"" lineType=""dashDot"" filled=""true"" center=""10; 10"" width=""20"" height=""30"" rotateAngle=""10"" />
                </shapes>";

            XmlLoader loader = new XmlLoader();

            //Act
            Shape[] shapes = loader.Load(xml);
            ShapeDrawer.Model.Shapes.Rectangle rectangle = shapes.Single() as ShapeDrawer.Model.Shapes.Rectangle;

            //Assert
            Assert.IsNotNull(rectangle, "Parsed shape is not Rectangle");

            Assert.AreEqual(Color.FromArgb(127, 255, 0, 255), rectangle.Color);
            Assert.AreEqual(ELineType.DashDot, rectangle.LineType);
            Assert.AreEqual(true, rectangle.Filled);
            Assert.AreEqual(new PointF(10, 10), rectangle.Center);
            Assert.AreEqual(10, rectangle.RotateAngle);
        }

        [TestMethod]
        public void LoadsMultipleShapesCorrectly()
        {
            //Arrange
            string xml = @"
                <shapes>
                    <line color=""127; 255; 255; 255"" lineType=""solid"" a=""-1,5; 3,4"" b=""2,2; 5,7"" />
                    <circle color=""127; 255; 255; 255"" lineType=""solid"" filled=""false"" center=""2,2; 5,7"" radius=""10"" />
                    <triangle color=""127; 255; 255; 255"" lineType=""solid"" filled=""true"" a=""-15; -20"" b=""15; -20,3"" c=""0; 21"" />
                    <rectangle color=""127; 255; 0; 255"" lineType=""dashDot"" filled=""true"" center=""10; 10"" width=""20"" height=""30"" rotateAngle=""0"" />
                </shapes>";
            XmlLoader loader = new XmlLoader();

            //Act
            Shape[] shapes = loader.Load(xml);

            //Assert
            Assert.AreEqual(4, shapes.Length);
            Assert.IsInstanceOfType(shapes[0], typeof(Line));
            Assert.IsInstanceOfType(shapes[1], typeof(Circle));
            Assert.IsInstanceOfType(shapes[2], typeof(Triangle));
            Assert.IsInstanceOfType(shapes[3], typeof(ShapeDrawer.Model.Shapes.Rectangle));
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void ThrowsIfEmptyStringPassed()
        {
            //Arrange
            XmlLoader loader = new XmlLoader();

            //Act
            loader.Load("");

            //Assert
            Assert.Fail("No exception thrown for empty xml input");
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void ThrowsIfNullXmlPassed()
        {
            //Arrange
            XmlLoader loader = new XmlLoader();

            //Act
            loader.Load(null);

            //Assert
            Assert.Fail("No exception thrown for null xml input");
        }

        [TestMethod]
        [ExpectedException(typeof(ParseException))]
        public void ThrowIfRequiredFieldMissing()
        {
            //Arrange
            //lineType missing
            string xml = @"
                <shapes>
                    <rectangle color=""127; 255; 0; 255"" filled=""true"" center=""10; 10"" width=""20"" height=""30"" rotateAngle=""0"" />
                </shapes>";

            XmlLoader loader = new XmlLoader();

            //Act
            loader.Load(xml);

            //Assert
            Assert.Fail("No exception thrown for missing required field");
        }
    }
}
